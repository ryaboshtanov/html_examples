﻿var user_name = $(".user-name-block h1").html().slice(0, 1);
$(".first_letter").html(user_name);
$(".nav-tabs a").click(function () {
    $(".tab-content").slideUp(200);
    $(".tab-content").slideDown(200);
});
$(document).ready(function() {
    $("body").addClass("js-start-animation");
    $(".small-user-avatar").addClass("js-start-animation");
    setTimeout(
    function () {
        $(".avatar-circle3").addClass("js-start-animation");
        $(".user-name-block h1").addClass("js-start-animation");
    }, 200);
    setTimeout(
    function () {
        $(".avatar-circle2").addClass("js-start-animation");
    }, 300);
    setTimeout(
    function () {
        $(".avatar-circle1").addClass("js-start-animation");
        $(".user-occupation-block h2").addClass("js-start-animation");
    }, 400);
    setTimeout(
    function () {
        $(".tabs-with-content").addClass("js-start-animation");
        $(".first_letter").addClass("js-start-animation");
    }, 1000);
    setTimeout(
    function () {
        $("body").addClass("normal-body");
    }, 2000);
    setTimeout(
    function () {
        $(".js-btn-animation-1").addClass("js-start-animation");
    }, 2200);
    setTimeout(
    function () {
        $(".js-btn-animation-2").addClass("js-start-animation");
    }, 2300);
    setTimeout(
    function () {
        $(".js-btn-animation-3").addClass("js-start-animation");
    }, 2400);
    setTimeout(
    function () {
        $(".js-btn-animation-4").addClass("js-start-animation");
    }, 2500);
    setTimeout(
    function () {
        $(".js-btn-animation-5").addClass("js-start-animation");
    }, 2600);
    setTimeout(
    function () {
        $(".js-btn-animation-6").addClass("js-start-animation");
    }, 2700);
});

$('[data-toggle="modal"]').on('click',
function (e) {
    $('#myModal').remove();
    e.preventDefault();
    var $this = $(this)
        , $remote = $this.data('remote') || $this.attr('href')
        , $modal = $('<div class="modal fade" id="ajaxModal"  role="dialog"><div class="modal-body"></div></div>');
    $('body').append($modal);
    $modal.modal({ keyboard: false });
    $modal.load($remote);
});
function initializeMap() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(44.5403, -78.5463),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
    map.set('styles', [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#0b97ed"
            },
            {
                "visibility": "on"
            }
        ]
    }
    ]);
}
google.maps.event.addDomListener(window, 'load', initializeMap);
$(".nav-tabs a").click(function () {
    if ($(this).attr("aria-controls") == "contacts") {
        $(".map-row").addClass("show");
    } else {
        $(".map-row").removeClass("show");
    }
});